import { Image, StyleSheet } from 'react-native'
import React from 'react'

export default function Logo() {
//   return <Image source={require("../assets/react-native-logo.png")} style={styles.image}/>;
    return <Image source={{uri:'https://www.loopstudio.dev/wp-content/uploads/2020/05/react-native-logo.png'}}
    style={{width:100, height:100}}/>
}