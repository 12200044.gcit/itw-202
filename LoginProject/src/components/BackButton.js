import { TouchableOpacity, Image, StyleSheet } from 'react-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import React from 'react'

export default function BackButton({goBack}) {
  return (
    <TouchableOpacity onPress={goBack} style={styles.container}>
        {/* <Image
        style={styles.image}
        source={require("../../assets/arrow_back")}
        /> */}
        <Image source={{uri:'https://flyclipart.com/thumb2/arrow-back-direction…eft-navigation-arrow-pointing-pointing-168000.png'}}/>

    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        top: 10 + getStatusBarHeight(),
        left:4,
    },
    image:{
        width:24,
        height:24,
    },
});