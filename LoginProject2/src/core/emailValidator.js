export function emailValidator(email){
    const re=/\S+@\S+\.\S+/;
    if(!email) return "Email cant be empty."
    if(!re.test(email)) return "Ooops! we need a valid email address."
    return "";
}