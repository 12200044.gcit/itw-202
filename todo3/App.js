import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {add, multiply} from '../todo3/component/nameExport';
import divide from '../todo3/component/defaultExport';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <Text>Result of addition: {add(5,6)}</Text>
      <Text>Result of Multiplication: {multiply(5,6)}</Text>
      <Text>Result of divide: {divide(10,2)}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
