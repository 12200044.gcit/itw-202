import React from "react";
// Named export
//my-math-func1.js
function add(x,y){
    return x+y;
}
function multiply(x,y){
    return x*y;
}
export{ add, multiply};