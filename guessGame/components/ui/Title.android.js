import React from "react";
import { StyleSheet,Text, View, Platform } from "react-native";
import Colors from "../../constants/Colors";

const Title=({children})=>{
    return(
        <Text style={styles.title}>{children}</Text>

    )
}
export default Title;

const styles= StyleSheet.create({
    title:{
        fontFamily: 'open-sans-bold',
    //   borderWidth: Platform.OS === 'android' ? 2: 0,
    // borderWidth: Platform.select({ ios:0, android:2}),
   
    borderRadius:5,
      borderColor: 'white',
      textAlign:'center',
      color: 'white',
      fontSize: 24, 
      maxWidth: '80%',
      padding:12,
      width:300
    }
})