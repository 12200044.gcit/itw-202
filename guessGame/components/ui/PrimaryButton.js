import { Pressable, StyleSheet, Text, View } from 'react-native'
import React from 'react'
function PrimaryButton({children, onPress}){
  return (

    <View style={styles.buttonOuterContainer}>
    <Pressable android_ripple={{color: 'blue'}} 
    style={({pressed}) => pressed ? 
            [styles.buttonInnerContainer, styles.pressed]:
            styles.buttonInnerContainer}
            onPress={onPress}>
    
      <Text style={styles.buttonText}>{children}</Text>
    
    </Pressable>
    </View>
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonOuterContainer:{
        backgroundColor: '#72063c',
        overflow:'hidden',
        borderRadius: 6,
        paddingVertical:8,
        paddingHorizontal: 16,
        elevation:2,
        margin:4,
    },
    buttonInnerContainer:{
        backgroundColor: '#72063c',
        borderRadius: 6,
        paddingVertical: 8,
        paddingHorizontal: 16,
        elevation: 2,
    },
    buttonText:{
        color: 'white',
        textAlign: 'center',
    },
    pressed:{
        opacity: 0.75,
    },

})