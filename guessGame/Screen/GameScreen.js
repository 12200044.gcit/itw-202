import React, {useEffect, useState} from 'react'
import { View, Text, StyleSheet, FlatList, useWindowDimensions } from 'react-native'
import { Alert } from 'react-native';
import NumberContainer from '../components/game/NumberContainer';
import Card from '../components/ui/Cards';
import InstructionText from '../components/ui/InstructionText';
import PrimaryButton from '../components/ui/PrimaryButton';
import Title from '../components/ui/Title.android'
import {Ionicons} from '@expo/vector-icons'
import GuessLogItem from '../components/game/GuessLogItems';

function  generateRandomBetween(min, max, exclude){
  const rndNum= Math.floor(Math.random()*(max-min))+min;

  if(rndNum == exclude){
    return generateRandomBetween(min, max, exclude)
  }
  else {
    return rndNum;
  }
}
let minBoundary=1;
let maxBoundary=100;
function GameScreen ({userNumber, onGameOver}) {

  const initialGuess=generateRandomBetween( 1, 100, userNumber)
  const [currentGuess, setCurrentGuess] = useState(initialGuess)
  const [guessRounds, setGuessRounds]=useState([initialGuess]);
  const {width, height}=useWindowDimensions();
useEffect(()=>{
  if (currentGuess === userNumber){
      onGameOver(guessRounds.length);
  }
},[currentGuess,userNumber,onGameOver])

useEffect(()=>{
  minBoundary=1;
  maxBoundary=100;
},[])
  function nextGuessHandler(direction){
    if(
      (direction === 'lower' && currentGuess < userNumber) ||
      (direction === 'greater' && currentGuess > userNumber))
      {
        Alert.alert("Don't lie!", 'You know that this wrong...',[
          {text: 'Sorry', style: 'cancel'}
        ])
        return;
    }



    if (direction === 'lower'){
      maxBoundary = currentGuess;
    }
    else{
      minBoundary = currentGuess + 1 ;
    }
    console.log(minBoundary, maxBoundary)
    const newRndNumber = generateRandomBetween(minBoundary, maxBoundary, currentGuess)

    setCurrentGuess(newRndNumber);
    setGuessRounds((pervGuessRounds => [newRndNumber, ...pervGuessRounds]))
    
  }
  const guessRoundsListLength=guessRounds.length


  let content=
    <>
      <NumberContainer>{currentGuess}</NumberContainer>
        <Card>

            <InstructionText style={styles.instructionText}>Higher or lower</InstructionText>
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this,'greater')}>
            <Ionicons name='md-add' size={24} color='white' />
          </PrimaryButton>
          </View>
          <View style={styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this,'lower')}>
          <Ionicons name='md-remove' size={24} color='white' />
          </PrimaryButton>
          </View>
    </View>
    </Card>
    </>
    
    if(width>500)
    {
      content=(
        <>
             
        <View style={styles.buttonsContainerWidth}> 
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this,'greater')}>
            <Ionicons name='md-add' size={24} color='white' />
          </PrimaryButton>
          </View>
          <NumberContainer>{currentGuess}</NumberContainer>
          <View style={styles.buttonContainer}>
          <PrimaryButton onPress={nextGuessHandler.bind(this,'lower')}>
          <Ionicons name='md-remove' size={24} color='white' />
          </PrimaryButton>
          </View>
        </View>
          
          
          
    
   
    </>
        
      )

    }

  
  
  return (
    <View style={styles.screen}>
            
        <Title>Opponents Guess</Title>
        {content}
   
    <View style={styles.listContainer}>
      <FlatList
      data={guessRounds}
      renderItem={(itemData)=> <GuessLogItem
      roundNumber={guessRoundsListLength - itemData.index}
      guess={itemData.item}
      />
    }
      keyExtractor={(item) => item}
      />
    </View>
    </View>
  )
}
export default GameScreen;

const styles = StyleSheet.create({
  buttonsContainerWidth:{
    flexDirection:'row',
    alignItems:'center',
    padding:10
  },
  screen:{
  flex: 1,
  padding: 24,
  alignItems:'center'
  },
  listContainer:{
    flex:1,
    padding:12
  },
  instructionText:{
    marginBottom:12
  },
  buttonsContainer:{
    flexDirection:'row'
  },
  buttonContainer:{
    flex:1
  },
  guess: {
  borderWidth: 2,
  borderColor: 'yellow',
  padding: '5%',
  textAlign:'center',
  justifyContent: 'center',
  
  },
  text:{
  color: 'yellow',
  fontSize: 24,
  textAlign: 'center',
  }
})
