import { StatusBar } from 'expo-status-bar';
import { StyleSheet } from 'react-native';
import { useState} from 'react';
import StartScreen from './Screen/StartScreen';
import { LinearGradient } from 'expo-linear-gradient';
import GameOverScreen from './Screen/GameOverScreen';
import GameScreen from './Screen/GameScreen';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';

export default function App() {
  const [userNumber, setUserNumber] =useState();
  const [gameIsOver,setGameIsOver]=useState(true);
  const [guessRounds, setGuessRounds]= useState(0)
 
 const [fontsLoaded] = useFonts({
    'open-sans': require('./assets/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/OpenSans-Bold.ttf'),
  })

  if (!fontsLoaded){
    return <AppLoading/>
  }

  function pickerNumberHandeler(pickerNumber){
    setUserNumber(pickerNumber);
    setGameIsOver(false);
  }
  function gameOverHandler(numberOfRounds){
    setGameIsOver(true);
    setGuessRounds(numberOfRounds)
  }
  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);
  }
  let screen =<StartScreen onPickNumber={pickerNumberHandeler}/>

  if(userNumber){
      screen =<GameScreen userNumber={userNumber} onGameOver={gameOverHandler} />
  }

  if (gameIsOver && userNumber){
    screen=<GameOverScreen
            userNumber={userNumber}
            roundsNumber={guessRounds}
            onStartNewGame={startNewGameHandler}
           />
   
  }
  return (
    <>
     <LinearGradient style={styles.container} colors={['#4c0329','#ddb52f']}>
    {screen}
    <StatusBar style="dark" />
  </LinearGradient>
    </>
   
  );
}

const styles = StyleSheet.create({
  container: {
  //  backgroundColor:'yellow',

   flex:1,
  },
});