import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyButtons from './component/ButtonExample';

export default function App() {
  return (
    <View style={styles.container}>
   
  <MyButtons buttontext='PRESS ME'></MyButtons>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
