import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style = {styles.container}>
      <View style = {styles.box1}></View>  
      <View style = {styles.box2}></View>
     
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    marginTop: 30,
    justifyContent: 'flex-end',
    marginLeft: 95,
    marginBottom:60

   },
   box1: {
    height:175,
    width:175,
    backgroundColor: "red",
  },
  box2: {
    height:175,
    width:175,
    backgroundColor: "blue",
  },
  box3: {
    height:250,
    width:25,
    backgroundColor: "green",
  },
 
});
